pub mod api;
pub mod fw;
pub use crate::fw::res::result::Res;

mod app_hdl {
    pub use crate::fw::res::result::{AppResult, Res};
    pub use crate::fw::util::page::MyPage;
}

mod app_stc {
    pub use crate::fw::util::page::PageParms;
    pub use serde::{Deserialize, Serialize};
    pub use sqlx::FromRow;
}

mod app_dao {
    pub use crate::fw::{res::result::AppResult, util::page::PageParms};
    pub use sqlx::{query_scalar, Executor, Sqlite};
}
