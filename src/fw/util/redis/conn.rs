use super::client::RedisClient;

use redis::{from_redis_value, ConnectionLike, FromRedisValue, RedisError, RedisResult};
use tracing::info;

pub enum RedisConnection {
    Single(Box<redis::Connection>),
    Cluster(Box<redis::cluster::ClusterConnection>),
}

impl RedisConnection {
    pub fn is_open(&self) -> bool {
        return match self {
            RedisConnection::Single(sc) => sc.is_open(),
            RedisConnection::Cluster(cc) => cc.is_open(),
        };
    }

    pub fn query<T: FromRedisValue>(&mut self, cmd: &redis::Cmd) -> RedisResult<T> {
        return match self {
            RedisConnection::Single(sc) => match sc.as_mut().req_command(cmd) {
                Ok(val) => {
                    // tracing::info!("111111");
                    // from_redis_value(&val)

                    match val {
                        redis::Value::Nil => {
                            // Handle the case where the key does not exist in Redis
                            info!("emptykey");
                            Err(redis::RedisError::from((
                                redis::ErrorKind::TypeError,
                                "emptykey",
                            )))
                        }
                        _ => {
                            // Implement your custom logic to convert the Redis value to YourStruct
                            // For example, you might use v.as_str() or v.as_integer() here
                            // Return Ok(YourStruct) if successful
                            // unimplemented!("Implement your conversion logic here")
                            info!("Value is not nil");
                            from_redis_value(&val)
                        }
                    }
                }
                Err(e) => Err(e),
            },
            RedisConnection::Cluster(cc) => match cc.req_command(cmd) {
                Ok(val) => from_redis_value(&val),
                Err(e) => Err(e),
            },
        };
    }
}

#[derive(Clone)]
pub struct RedisConnectionManager {
    pub redis_client: RedisClient,
}

impl r2d2::ManageConnection for RedisConnectionManager {
    type Connection = RedisConnection;
    type Error = RedisError;

    fn connect(&self) -> Result<RedisConnection, Self::Error> {
        let conn = self.redis_client.get_redis_connection()?;
        Ok(conn)
    }

    fn is_valid(&self, conn: &mut RedisConnection) -> Result<(), Self::Error> {
        match conn {
            RedisConnection::Single(sc) => {
                redis::cmd("PING").query(sc)?;
            }
            RedisConnection::Cluster(cc) => {
                redis::cmd("PING").query(cc)?;
            }
        }
        Ok(())
    }

    fn has_broken(&self, conn: &mut RedisConnection) -> bool {
        !conn.is_open()
    }
}
