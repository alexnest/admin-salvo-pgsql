pub mod client;
pub mod conn;
pub mod instance;

use anyhow::Result;
use conn::RedisConnectionManager;
use instance::RedisInstance;
use r2d2::Pool;
use std::time::Duration;

pub struct PoolCof {
    max_size: u32,
    min_idle: Option<u32>,
    connection_timeout: Duration,
}

pub struct PoolCofBuilder {
    max_size: u32,
    min_idle: u32,
    connection_timeout: u64,
}

impl PoolCofBuilder {
    pub fn new() -> Self {
        Self {
            max_size: 10,
            min_idle: 3,
            connection_timeout: 60,
        }
    }

    pub fn max_size(mut self, max_size: u32) -> Self {
        self.max_size = max_size;
        self
    }

    pub fn min_idle(mut self, min_idle: u32) -> Self {
        self.min_idle = min_idle;
        self
    }

    pub fn connection_timeout(mut self, connection_timeout: u64) -> Self {
        self.connection_timeout = connection_timeout;
        self
    }

    pub fn build(self) -> PoolCof {
        PoolCof {
            max_size: self.max_size,
            min_idle: Some(self.min_idle),
            connection_timeout: Duration::from_secs(self.connection_timeout),
        }
    }
}

pub fn gen_redis_conn_pool(
    instance: RedisInstance,
    pool_conf: PoolCof,
) -> Result<Pool<RedisConnectionManager>> {
    let redis_client = instance.to_redis_client()?;
    let manager = RedisConnectionManager { redis_client };
    let pool = r2d2::Pool::builder()
        .max_size(pool_conf.max_size)
        .min_idle(pool_conf.min_idle)
        .connection_timeout(pool_conf.connection_timeout)
        .build(manager)?;
    Ok(pool)
}
