use redis::{cluster::ClusterClientBuilder, RedisResult};

use super::client::RedisClient;

pub enum InstanceType {
    Single,
    Cluster,
}

pub struct RedisInstance {
    pub urls: Vec<String>,
    pub pwd: String,
    pub instance_type: InstanceType,
}

impl Default for RedisInstance {
    fn default() -> Self {
        Self {
            urls: vec!["redis://127.0.0.1:6379".to_string()],
            pwd: "".to_string(),
            instance_type: InstanceType::Single,
        }
    }
}

impl RedisInstance {
    pub fn to_redis_client(&self) -> RedisResult<RedisClient> {
        return match self.instance_type {
            InstanceType::Single => {
                let cl = redis::Client::open(self.urls[0].as_str())?;
                Ok(RedisClient::Single(cl))
            }
            InstanceType::Cluster => {
                let mut cb = ClusterClientBuilder::new(self.urls.clone());
                if !self.pwd.is_empty() {
                    cb = cb.password(self.pwd.clone());
                }
                let cl = cb.build()?;
                Ok(RedisClient::Cluster(cl))
            }
        };
    }
}

pub struct RedisInstanceBuilder {
    urls: Vec<String>,
    pwd: String,
    instance_type: InstanceType,
}

impl RedisInstanceBuilder {
    pub fn new() -> Self {
        Self {
            urls: vec!["redis://127.0.0.1:6379".to_string()],
            pwd: "".to_string(),
            instance_type: InstanceType::Single,
        }
    }

    pub fn urls(mut self, urls: Vec<String>) -> Self {
        self.urls = urls;
        self
    }

    pub fn pwd(mut self, pwd: String) -> Self {
        self.pwd = pwd;
        self
    }

    pub fn instance_type(mut self, instance_type: InstanceType) -> Self {
        self.instance_type = instance_type;
        self
    }

    pub fn build(self) -> RedisInstance {
        RedisInstance {
            urls: self.urls,
            pwd: self.pwd,
            instance_type: self.instance_type,
        }
    }
}
