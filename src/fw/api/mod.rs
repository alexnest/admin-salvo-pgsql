pub mod auth;

use auth::{captcha, login};
use salvo::Router;

pub struct Routes;

impl Routes {
    pub fn new() -> Router {
        Router::new().push(
            Router::with_path("auth")
                .push(Router::with_path("captcha").get(captcha::get_captcha))
                .push(Router::with_path("login").post(login::login)),
        )
    }
}
