use bcrypt;
use salvo::prelude::*;
use serde::Deserialize;

use crate::{
    app_hdl::*,
    fw::{cache, global::prefix::CAPTCHA},
};

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct LoginInfo {
    account: String,
    pwd: String,
    captcha: String,
    captcha_uuid: String,
}

#[handler]
pub async fn login(
    req: &mut Request,
    _depot: &mut Depot,
    res: &mut Response,
    _ctrl: &mut FlowCtrl,
) -> AppResult<()> {
    let param = req.parse_json::<LoginInfo>().await?;
    let captcha = cache::get_u32(CAPTCHA, &String::from(param.captcha_uuid)).await?;
    println!("captcha: {:?}", captcha);
    // match cache {
    //     Some(code) => {
    //         if code.to_string() != param.captcha {
    //             Res::fail::<()>().msg("code error").render(res);
    //             return Ok(());
    //         }
    //     }
    //     None => {
    //         Res::fail::<()>().msg("code invalid").render(res);
    //         return Ok(());
    //     }
    // }

    Ok(())
}
