use anyhow::{anyhow, Error, Result};
use once_cell::sync::OnceCell;
use tracing::info;

use crate::{
    app_hdl::AppResult,
    fw::{
        res::error::AppError,
        util::redis::{
            conn::RedisConnectionManager, gen_redis_conn_pool, instance::RedisInstanceBuilder,
            PoolCofBuilder,
        },
    },
};

pub static GLOBAL_CACHE: OnceCell<r2d2::Pool<RedisConnectionManager>> = OnceCell::new();

pub async fn init_cache() {
    GLOBAL_CACHE.get_or_init(|| {
        let instance = RedisInstanceBuilder::new().pwd("".to_string()).build();
        let pool_cof = PoolCofBuilder::new()
            .min_idle(3)
            .max_size(10)
            .connection_timeout(60)
            .build();
        let pool = match gen_redis_conn_pool(instance, pool_cof) {
            Ok(it) => it,
            Err(err) => panic!("{}", err.to_string()),
        };
        pool
    });
}

pub async fn set_u32(prefix: &str, key: &str, val: u32) -> Result<()> {
    let conn = GLOBAL_CACHE.get();
    return match conn {
        Some(c) => {
            let key = format!("{}_{}", prefix, key);
            c.get()?.query(redis::cmd("set").arg(key).arg(val))?;
            Ok(())
        }
        None => Err(anyhow!("redis pool not init")),
    };
}

pub async fn set_u32_with_expiry(
    prefix: &str,
    key: &str,
    val: u32,
    expiry_seconds: usize,
) -> Result<()> {
    let conn = GLOBAL_CACHE.get();
    info!("set_u32_with_expiry");
    return match conn {
        Some(c) => {
            let key = format!("{}_{}", prefix, key);
            info!("set key: {}", key);
            c.get()?.query(
                redis::cmd("set")
                    .arg(key)
                    .arg(val)
                    .arg("EX")
                    .arg(expiry_seconds),
            )?;
            Ok(())
        }
        None => Err(anyhow!("redis pool not init")),
    };
}


// pub async fn get_u32(prefix: &str, key: &str) -> Result<u32> {
//     let conn = GLOBAL_CACHE.get();
//     return match conn {
//         Some(c) => {
//             let key = format!("{}_{}", prefix, key);
//             info!("get key: {} ", key);
//             let result: Option<u32> = c.get()?.query(redis::cmd("get").arg(key))?;
//             match result {
//                 Some(val) => Ok(val),
//                 None => return Err(anyhow!("get val is none")),
//             }
//         }
//         None => return Err(anyhow!("redis pool not init")), //AppError::Other(anyhow!("redis pool not init")),
//     };
// }

pub async fn get_u32(prefix: &str, key: &str) -> AppResult<u32> {
    let conn = GLOBAL_CACHE.get();
    return match conn {
        Some(c) => {
            let key = format!("{}_{}", prefix, key);
            info!("get key: {} ", key);
            let result: Option<u32> = c.get()?.query(redis::cmd("get").arg(key))?;
            match result {
                Some(val) => Ok(val),
                None => Err(AppError::Other(anyhow!("get val is none"))),
            }
        }
        None => Err(AppError::Other(anyhow!("redis pool not init"))), //AppError::Other(anyhow!("redis pool not init")),
    };
}
