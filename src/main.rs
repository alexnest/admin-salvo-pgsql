use admin_salvo_pgsql::fw::{
    cache::init_cache,
    catcher::handle_invalid_http_code,
    config::APPLICATION_CONFIG,
    log::init_log,
    api
};
use salvo::{catcher::Catcher, prelude::*};
use tracing::info;

// you can see the log when you visit the web url 'http://127.0.0.1:5800/'
#[handler]
async fn hello() -> &'static str {
    tracing::info!("hello");
    tracing::info!("test");
    "Hello World"
}

#[tokio::main]
async fn main() {
    // init log
    init_log();
    info!("init log complete!");

    // init cache
    init_cache().await;
    info!("init cache complete!");

    // init router
    let router = Router::new()
        .push(Router::with_path("api").push(api::Routes::new()))
        .push(Router::new().get(hello));
    info!("init router complete!");

    // init service
    let service = Service::new(router).catcher(Catcher::default().hoop(handle_invalid_http_code));
    info!("init service complete!");

    // init acceptor
    let acceptor = TcpListener::new(APPLICATION_CONFIG.server.host.as_str())
        .bind()
        .await;
    info!("init acceptor complete!");

    info!("server ready to start!");

    // start server
    Server::new(acceptor).serve(service).await;
}
